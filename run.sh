#!/bin/bash

. /opt/conda/etc/profile.d/conda.sh
conda activate daiquiri

set -e

if [ -t 0 ] ; then
    supervisord &

    export TANGO_HOST=localhost:20000
    export BEACON_HOST=localhost

    CMD="bliss -s test_session"
    if [ "$1" ]
    then
        CMD=$@
    fi

    # Wait for beacon
    sleep 5

    $CMD
else
    supervisord
fi
