#!/bin/bash
if [ ! -d node_modules/\@esrf-ui/redux-provider/dist ]; then
    git clone https://gitlab.esrf.fr/ui/redux-provider
    cd redux-provider
    npm install
    npm run build
    cd ..
    mv redux-provider/dist node_modules/\@esrf-ui/redux-provider/
    rm -rf redux-provider
    rm install_redux_provider.sh
fi
