#!/bin/bash
. /opt/conda/etc/profile.d/conda.sh
conda activate daiquiri

export TANGO_HOST=localhost:20000
python /bliss/bliss/testutils/servers/dummy_tg_server.py dummy
