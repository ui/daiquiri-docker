#!/bin/bash
. /opt/conda/etc/profile.d/conda.sh
conda activate daiquiri

export TANGO_HOST=localhost:20000
# RegisterNexusWriter test_session domain id00 --instance nexuswriters
NexusWriterService nexuswriters --log=info
