#!/bin/bash
. /opt/conda/etc/profile.d/conda.sh
conda activate daiquiri

python << EOF
import tango
d = tango.DeviceProxy("localhost:20000/id00/limaccds/simulator1")
d.video_live = 1
print(d.video_live)
EOF
