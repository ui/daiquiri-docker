#!/bin/bash
. /opt/conda/etc/profile.d/conda.sh
conda activate daiquiri

RESOURCES="tests/resources"
if [ "$1" ]
then
    RESOURCES=$1
fi

STATIC="/daiquiri-ui/build"
if [ "$2" ]
then
    STATIC=$2
fi

daiquiri-server --resource-folders=$RESOURCES --static-folder=$STATIC
