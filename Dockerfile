FROM node:16 as builder

WORKDIR /app
RUN git clone https://gitlab.esrf.fr/ui/daiquiri-ui.git . && \
    npm install -g pnpm@8 && \
    pnpm install && \
    pnpm run build

FROM condaforge/mambaforge:24.9.2-0

# Set timezone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Update and basic install
RUN apt-get update && \
    apt-get install -y --no-install-recommends tzdata curl wget bzip2 build-essential libgl1-mesa-glx xvfb libxrender1 git redis-server mariadb-client && \
    apt-get clean autoclean && apt-get autoremove --yes && \
    rm -rf /var/lib/apt/lists/*

# Clone repos
WORKDIR /
RUN git clone https://gitlab.esrf.fr/ui/daiquiri.git && \
    git clone https://gitlab.esrf.fr/ui/video-streamer-mpeg.git && \
    git clone https://gitlab.esrf.fr/bliss/bliss.git && \
    git clone https://gitlab.esrf.fr/bliss/blissoda.git

# Setup environment
RUN conda config --append channels esrf-bcu && \
    conda config --append channels tango-controls

WORKDIR /daiquiri
RUN ["/bin/bash", "-c", ". /opt/conda/etc/profile.d/conda.sh && \
    . /opt/conda/etc/profile.d/mamba.sh && \
    mamba init && \
    cd /bliss && \
    make dev_env NAME=daiquiri && \
    mamba activate daiquiri && \
    # Install bliss rest deps
    cd /bliss && pip install --no-cache-dir -e .[rest] && \
    pip install --no-cache-dir blisswebui && \
    # Install blissoda from source
    cd /blissoda && pip install --no-cache-dir -e .[client] && cd /bliss && \
    cd /daiquiri && \
    mamba install --file requirements-conda.txt && \
    mamba clean -afy && \
    # TODO: Temporary install
    pip install --no-cache-dir celery pytest-celery && \
    pip install --no-cache-dir -e .[doc] && \
    cd /video-streamer-mpeg && \
    mamba install --file requirements-conda.txt && mamba clean -afy && \
    pip install --no-cache-dir -e . && \
    pip install --no-cache-dir 'pytango==9.4.2' && \
    mkdir -p /var/log/bliss && \
    # Bliss makefile will have used pip cache
    rm -rf /root/.cache/pip && \
    # https://jcristharif.com/conda-docker-tips.html
    find /opt/conda/ -follow -type f -name '*.a' -delete && \
    find /opt/conda/ -follow -type f -name '*.pyc' -delete && \
    find /opt/conda/ -follow -type f -name '*.js.map' -delete"]

WORKDIR /daiquiri-ui
COPY --from=builder /app/build ./build

RUN ["/bin/bash", "-c", ". /opt/conda/etc/profile.d/conda.sh && \
    . /opt/conda/etc/profile.d/mamba.sh && \
    pip install --no-cache-dir pyzmq supervisor multivisor[web]"]
COPY supervisor/supervisord.conf /etc/supervisor/supervisord.conf

# Run
WORKDIR /daiquiri
ENV DAIQUIRI_RESOURCE_FOLDER /daiquiri/tests/resources
ENV DAIQUIRI_IMPLEMENTORS daiquiri.implementors
ENV DAIQUIRI_META_URL mariadb:3306/test
ENV USE_NEXUS_WRITER true

EXPOSE 8080 9030 9032 9034 5000 5001 6379 6380 25000

ENTRYPOINT ["/run.sh"]
COPY run.sh /

# Copy launchers
COPY launchers/daiquiri.sh /bin/
COPY launchers/shutter.sh /bin/
COPY launchers/lima.sh /bin/
COPY launchers/lima_live.sh /bin/
COPY launchers/nexuswriter.sh /bin/
RUN echo "mamba activate daiquiri" >> ~/.bashrc
COPY conda/env_vars.sh /opt/conda/envs/daiquiri/etc/conda/activate.d/
COPY blissterm/ /daiquiri/tests/bliss_configuration/blissterm/
COPY ewoks/ /daiquiri/tests/bliss_configuration/ewoks/

# Health check
COPY healthcheck.py /
HEALTHCHECK --interval=30s --timeout=10s --start-period=60s \
    CMD "mamba run -n daiquiri python /healthcheck.py"
