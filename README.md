# Daiquiri-docker

A docker image containing daiquiri's api, config, ui; bliss and some examples.

## Building

`--no-cache` ensures entire docker image is rebuilt (installing new deps, etc)

```bash
docker build --no-cache -t daiquiri:<release> .
```

You can set `<release>` to whatever indicator you see fit (number, commit hash...) or omit it. Just be sure to reuse to same in the commands below.

Prebuilt images are available from esrfbcu on dockerhub: https://hub.docker.com/r/esrfbcu/daiquiri

## Running

The test database from https://gitlab.esrf.fr/ui/mimosa/mimosa-database must be running:

```bash
docker run --name mariadb esrfbcu/mimosa-database:main
```

Start daiquiri docker and launch bliss session:

```bash
docker run --rm --link mariadb -p 9032:9032 -p 9030:9030 -p 8080:8080 esrfbcu/daiquiri:main
```

The image contains the lastest master of daiquiri, but this can be overridden by a local copy by mounting into `/daiquiri`.

By default, data will be saved to `/data`. If you want to make this available outside of the container, mount `/data`

```bash
docker run -v "/data:/data" -v "/path/to/daiquiri:/daiquiri" --rm --link mariadb -p 9032:9032 -p 9030:9030 -p 8080:8080 daiquiri:<release>
```

### Interactive Mode

Run in interactive mode to be dropped in to a bliss cli test_session

```bash
docker run -v "/data:/data" --rm --link mariadb -p 9032:9032 -p 9030:9030 -p 8080:8080 -it daiquiri:<release>
```

By default the container will launch `bliss -s test_session`, pass a final argument to docker run to override this

```bash
docker run -v "/data:/data" --rm --link mariadb -p 9032:9032 -p 9030:9030 -p 8080:8080 -it daiquiri:<release> /bin/bash
```

### Ports

The following ports can be mapped

| Port | Service                  |
| ---- | ------------------------ |
| 8080 | Daiquiri web server      |
| 5001 | Video streamer           |
| 9030 | Beacon configuration app |
| 9032 | Supervisor               |
| 9034 | Multivisor RPC           |
| 5000 | Blissterm                |
| 6380 | Redis scan data          |

## Supervisor

The image comes with supervisor and is configured to start:

- beacon
- nexuswriters
- daiquiri
- lima_simulator
- tango_dummy

These processes can also be manually started using the included launchers.

## Manual Launchers

Start daiquiri, which accepts an optional path to resources to customise loading of daiquiri

```bash
$ ./connect.sh
docker# daiquiri.sh [path/to/resources/folder]
```

Start optionals

```bash
$ ./connect.sh
docker# lima.sh
```

Nexuswriter is autostarted with bliss but can also be started manually via:

```bash
$ ./connect.sh
docker# nexuswriter.sh
```

## Bliss

Bliss is mounted into /bliss and can be overwritten by a local mount as follows:

```bash
docker run -v "/path/to/bliss:/bliss" [...]
```

The Bliss package needs to be installed if mounting your own copy, the simplest way to do this is to start the container with bash

```bash
docker run -it /bin/bash
conda activate daiquiri
cd /bliss
pip install -e .
```

Now restart as normal. Be aware that this does not change the installed dependencies.

## Configuration

By default the daiquiri resources are loaded from `/daiquiri/tests/resources`, this can be
overridden by specifying the `DAIQUIRI_RESOURCE_FOLDER` environment variable.

The Bliss configuration is loaded from the test settings of daiquiri and can be found in:

```bash
/daiquiri/tests/bliss_configuration
```

It includes a basic test_session with motors, diode, mca, and lima simulator.
