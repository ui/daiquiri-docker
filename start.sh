#!/bin/bash
version=main
docker run \
    -d \
    # -v "/Users/Shared/data:/data" \
    # -v "$(pwd)/../../acq:/daiquiri" \
    --name daiquiri --link mariadb \
    # -e DAIQUIRI_RESOURCE_FOLDER="/daiquiri/local_config/resources" \
    # -e DISPLAY=host.docker.internal:0 \
    --cap-add SYS_PTRACE \
    -p 5000:5000 -p 5001:5001 -p 9010:9010 -p 9030:9030 -p 9032:9032 -p 9034:9034 -p 8080:8080 -p 6380:6380 \
    --add-host=daiquiri:127.0.0.1 \
    esrfbcu/daiquiri:${version}
