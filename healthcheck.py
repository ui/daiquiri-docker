#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import requests
import tango


if __name__ == "__main__":
    # supervisor
    resp = requests.get("http://localhost:9032")
    assert resp.status_code == 200

    # beacon
    resp = requests.get("http://localhost:9030")
    assert resp.status_code == 200

    # blissterm
    resp = requests.get("http://localhost:5000")
    assert resp.status_code == 200

    # daiquiri
    try:
        # For testing with local certificates
        resp = requests.get(
            "https://localhost:8080/api/components/config", verify=False
        )
    except requests.exceptions.SSLError:
        resp = requests.get("http://localhost:8080/api/components/config")

    assert resp.status_code == 200

    # tango
    os.environ["TANGO_HOST"] = "localhost:20000"
    lima = tango.DeviceProxy("id00/limaccds/simulator1")
    assert lima.state() == tango.DevState.ON

    dummy = tango.DeviceProxy("id00/tango/dummy")
    assert dummy.state() in [tango.DevState.OPEN, tango.DevState.CLOSE]
